"""
Construir un programa en Python que sirva cualquier invocación que se le realice
con una redirección (códigos de resultado HTTP en el rango 3xx) a otro recurso (aleatorio),
que puede ser externa o a partir de una lista con URLs.
"""

import socket   #Importar la librería socket
import random   #Importar la librería random

urls = ["https://www.google.com", "https://www.instagram.com", "https://www.youtube.com"]

myPort = 1234   #Definimos el puerto como constante
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    #Creamos el socket
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  #Reutilizar puerto
mySocket.bind(('', myPort))  #Atamos el socket
mySocket.listen(5)  #Número de peticiones que pueden ser encoladas en el servidor
random.seed()   #Hace que cada vez se empiece por un número diferente de "la serie"

def process(request):   #Proceso que gestiona la petición del navegador y da una respuesta
    new_url = random.choice(urls)     # Elección aleatoria de la próxima URL
    response = "HTTP/1.1 301 Moved Permanently\r\n" #Le dice al navegador que la petición ha sido redirigida
    response += f"Location: {new_url}\r\n" #Dirección de la nueva URL
    response += "\r\n" #Final de la línea
    return response.encode('utf-8') #Codificamos la respuesta en bytes


try:  # Para controlar excepciones
    while True:  # Bucle sin fin característico de los servidores
        print(f"Waiting for connections")
        (recvSocket, address) = mySocket.accept()  # El servidor espera a una conexión
        print(f"HTTP request received:")  # Dirección del navegador que envía la petición
        request = recvSocket.recv(8 * 1024)  # Número máximo de datos que puede leer, guarda la petición
        print(request)  # El servidor imprime la petición del navegador
        response = process(request) #En el proceso se gestiona la respuesta del servidor al navegador que le ha pedido algo
        recvSocket.send(response)  # El servidor envía el string codificado al navegador
        recvSocket.close()  # El servidor cierra el socket

# Control de excepciones
except KeyboardInterrupt:
    print("Closing binded socket")
    mySocket.close()



